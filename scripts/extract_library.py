#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import glob
import os
import sys


class LibraryExtractor(object):
    def __init__(self):

        self.python_extensions = ['.py']
        self.cpp_extensions = ['.cpp', '.hpp']
        self.c_extensions = ['.c', '.h']

        self.target_extensions = \
            self.python_extensions \
            + self.cpp_extensions \
            + self.c_extensions

    def check_package(self, taregt_dir):
        if not os.path.isdir(taregt_dir):
            return False

        contents = glob.glob(taregt_dir + "/*")
        is_package = False
        for content in contents:
            if os.path.basename(content) == 'src' \
               or os.path.basename(content) == 'scripts' \
               or os.path.basename(content) == 'script':
                is_package = True
                break

        return is_package

    def get_package_paths(self, root_path):
        if self.check_package(root_path):
            return [root_path]

        candidates = \
            glob.glob(os.path.join(root_path, "*")) \
            + glob.glob(os.path.join(root_path, "*/*")) \
            + glob.glob(os.path.join(root_path, "*/*/*")) \
            + glob.glob(os.path.join(root_path, "*/*/*/*")) \
            + glob.glob(os.path.join(root_path, "*/*/*/*/*"))

        package_paths = []
        for candidate in candidates:
            if self.check_package(candidate):
                package_paths.append(candidate)

        package_paths.sort()
        return package_paths

    def check_source(self, path):
        if not os.path.isfile(path):
            return False

        for target_extension in self.target_extensions:
            if os.path.basename(path).endswith(target_extension):
                return True

        try:
            with open(path) as f:
                s = f.readlines()
                if s[0] == '#!/usr/bin/env python' \
                   or s[0] == '#!/usr/bin/env python2' \
                   or s[0] == '#!/usr/bin/env python3':
                    return True
        except Exception as e:
            print(e)

        return False

    def get_sources(self, root_path):
        sources = []

        if os.path.isfile(root_path):
            candidates = [root_path]
        else:
            candidates = \
                glob.glob(os.path.join(root_path, "*.*")) \
                + glob.glob(os.path.join(root_path, "*/*.*")) \
                + glob.glob(os.path.join(root_path, "*/*/*.*")) \
                + glob.glob(os.path.join(root_path, "*/*/*/*.*")) \
                + glob.glob(os.path.join(root_path, "*/*/*/*/*.*")) \
                + glob.glob(os.path.join(root_path, "*/*/*/*/*/*.*"))

        for candidate in candidates:
            if self.check_source(candidate):
                sources.append(candidate)
            else:
                continue

        sources.sort()

        return sources

    def extract_libraries_python(self, lines, file_path):
        libraries = []
        is_comment = False
        # check each lines
        for line in lines:
            # check if it is comment line
            if not is_comment:
                if line.startswith('#'):
                    continue
                elif line.startswith('"""') or line.startswith("'''"):
                    is_comment = True
                    continue
            else:
                if line.startswith('"""') or line.startswith("'''"):
                    is_comment = False
                    continue

            # end search when functions definitions
            if "def " in line:
                break

            # find library
            if "import" not in line:
                continue

            words = line.split(' ')
            for i, word in enumerate(words):
                if word == "from":
                    candidate = words[i + 1].split('.')[0].replace("\n", "")

                    ignore = False
                    private_libs = glob.glob(os.path.dirname(file_path) + "/*")
                    for lib in private_libs:
                        if os.path.basename(lib).split(".")[0] == candidate:
                            ignore = True
                            break
                    if ignore:
                        break
                    else:
                        libraries.append(candidate)
                    break

                if word == "import":
                    candidate = words[i + 1].split('.')[0].replace("\n", "")
                    libraries.append(candidate)
                    break

        return libraries

    def extract_libraries_cpp(self, lines):
        libraries = []
        is_comment = False
        # check each lines
        for line in lines:
            # print(line)
            # check if it is comment line
            if not is_comment:
                if line.startswith('//'):
                    continue
                elif line.startswith('/*'):
                    is_comment = True
                    continue
            else:
                if '*/' in line:
                    is_comment = False
                    continue

            # end search when functions definitions
            if "{" in line:
                break

            # find library
            if "#include" not in line:
                continue

            words = line.split(' ')
            for i, word in enumerate(words):
                if word == "#include":
                    candidate = words[i + 1].replace("\n", "")
                    if candidate.startswith('<') and candidate.endswith('>'):
                        candidate = candidate.replace('<', '').replace('>', '')
                        libraries.append(candidate.split('/')[0])
                    elif candidate.startswith('"') and candidate.endswith('"'):
                        pass
                    else:
                        libraries.append(candidate)
                        break

        return libraries

    def get_language(self, file_path):
        for target_extension in self.python_extensions:
            if os.path.basename(file_path).endswith(target_extension):
                return 'python'

        for target_extension in self.cpp_extensions:
            if os.path.basename(file_path).endswith(target_extension):
                return 'cpp'

        for target_extension in self.c_extensions:
            if os.path.basename(file_path).endswith(target_extension):
                return 'c'

        with open(file_path) as f:
            s = f.readlines()
            if s[0] == '#!/usr/bin/env python' \
               or s[0] == '#!/usr/bin/env python2' \
               or s[0] == '#!/usr/bin/env python3':
                return 'python'

        return 'not registered'

    def extract_libraries(self, file_path):
        try:
            language = self.get_language(file_path)
            with open(file_path) as f:
                lines = f.readlines()

                if language == 'python':
                    libraries = self.extract_libraries_python(lines, file_path)
                elif language == 'c' or language == 'cpp':
                    libraries = self.extract_libraries_cpp(lines)
                else:
                    print(file_path, " is not target.")
                    return []

        except Exception as e:
            print(e)
            return []

        return libraries

    def remove_hk_libraries(self, libraries):
        libs = []
        for lib in libraries:
            if not lib.startswith("hk_") \
               and not lib.startswith("tmc_") \
               and not lib.startswith("__future__"):
                libs.append(str(lib))

        libs.sort()
        return libs

    def save_output_csv(self, package_list, output_path):
        if os.path.exists(output_path):
            print("Output file exists. Overwritten.")

        print("Saveing ", output_path, "...")
        header = ['パッケージ',
                  'ライブラリ',
                  '正しいライブラリ名',
                  'ライセンス']
        with open(output_path, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(header)
            for package in package_list:
                writer.writerow([package['package']])
                for lib in package['libraries']:
                    writer.writerow(['', lib])
        print("Saved.")

        return

    def create_library_list(self, target_path, output_path):
        if not os.path.exists(target_path):
            print('Target does not exist: ', target_path)

        package_list = []
        package_paths = self.get_package_paths(target_path)
        for package in package_paths:
            libraries = []
            files = []
            sources = self.get_sources(package)
            for source in sources:
                libraries += self.extract_libraries(source)
                files.append(source.replace(target_path, ""))

            package_name = package.replace(target_path, "")
            print(package_name)
            libraries = self.remove_hk_libraries(set(libraries))
            print(libraries)
            # print(files)

            package_list.append({'package': package_name,
                                 'libraries': libraries,
                                 'files': files})

        self.save_output_csv(package_list, output_path)


if __name__ == '__main__':
    target_dir = "./"
    output_path = "./libraries.csv"

    if len(sys.argv) > 1:
        target_dir = sys.argv[1]
        if len(sys.argv) > 2:
            output_path = sys.argv[2]
            if not output_path.endswith('.csv'):
                print("出力パスは *.csv を指定してください")
                sys.exit(-1)

    extractor = LibraryExtractor()
    extractor.create_library_list(target_dir, output_path)
